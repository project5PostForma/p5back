package com.example.P5.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Lesson {
	
	@Id
	@GeneratedValue 
	private Long id;

	private Boolean isPayed;
	
	private Date date;
	

    
    @ManyToOne
	@JoinColumn(name = "user_id")
	@JsonBackReference
    private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
    
	public Lesson(){}
	
	public Lesson(Boolean isPayed, Date date, Long userId){
		this.isPayed = isPayed;
		this.date = date;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Boolean isPayed) {
		this.isPayed = isPayed;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	




}
