package com.example.P5.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class User {
	
	@Id
	@GeneratedValue 
	private Long id;

	private String name;
	
	private String firstname;
	
	private String phone;
	
	private String lessonDay;
	
	private Long price;
	
	private Number paymentStatus;

	private Number givenLessons;

	private String slot;
	
	private String lessonDuration;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<Lesson> lessons = new HashSet<>();
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<Rep> reps = new HashSet<>();
	
	public User() {}
	
	public User(String name, String firstname, String slot, String phone, String lessonDay, 
			Long price, String lessonDuration, Set<Lesson> lessons, Set<Rep> reps, Number paymentStatus, Number givenLessons) {
		this.name = name;
		this.firstname = firstname;
		this.slot = slot;
		this.lessonDay = lessonDay;
		this.slot = slot;
		this.price = price;
		this.lessonDuration = lessonDuration;
		this.lessons = lessons;
		this.reps = reps;
		this.paymentStatus = paymentStatus;
		this.givenLessons = givenLessons;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLessonDay() {
		return lessonDay;
	}

	public void setLessonDay(String day) {
		this.lessonDay = day;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getLessonDuration() {
		return lessonDuration;
	}

	public void setLessonDuration(String lessonDuration) {
		this.lessonDuration = lessonDuration;
	}

	public Set<Lesson> getLessons() {
		return lessons;
	}

	public void setLessons(Set<Lesson> lessons) {
		this.lessons = lessons;
	}

	public Set<Rep> getReps() {
		return reps;
	}

	public void setReps(Set<Rep> reps) {
		this.reps = reps;
	}

	public Number getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Number paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Number getGivenLessons() {
		return givenLessons;
	}

	public void setGivenLessons(Number givenLessons) {
		this.givenLessons = givenLessons;
	}
	
	


}
