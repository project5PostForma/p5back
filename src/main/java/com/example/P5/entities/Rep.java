package com.example.P5.entities;

import java.util.Date;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Rep {
	
	@Id
	@GeneratedValue 
	private Long id;

	private String title;
	
	private String compositor;
	
	private Date beginDate;
	

    
    @ManyToOne
	@JoinColumn(name = "user_id")
	@JsonBackReference
    private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
    
	public Rep(){}
	
	public Rep(String title, String compositor, Date beginDate, Long userId){
		this.title = title;
		this.compositor = compositor;
		this.beginDate = beginDate;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompositor() {
		return compositor;
	}

	public void setCompositor(String compositor) {
		this.compositor = compositor;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}





}
