package com.example.P5.services;


import com.example.P5.entities.Lesson;
import com.example.P5.entities.Rep;
import com.example.P5.entities.User;
import com.example.P5.repository.LessonRepo;
import com.example.P5.repository.RepRepo;
import com.example.P5.repository.UserRepo;

import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {
	
	@Autowired 
	private UserRepo userRepo;
	
	@Autowired 
	private LessonRepo lessonRepo;
	
	@Autowired 
	private RepRepo repRepo;
	
	public Iterable<User> findAllUsers() {
		return userRepo.findAll();
	}
	
	public User findUser(Long id) {
		return userRepo.findById(id).get(); 
	}

	public void addUser(User newUser) {
		userRepo.save(newUser);
	}

	public void deleteUserById(Long id) {
    	User user = userRepo.findById(id).get(); 
        userRepo.delete(user);
	}
	
    public void modifyUser(Long id, User updatedUser) {
    	System.out.println("modify");
    	User user = userRepo.findById(id).get(); // optionnal pour check que c'est pas null, get si on est sûr
    	user.setFirstname(updatedUser.getFirstname());
    	user.setName(updatedUser.getName());
		user.setPhone(updatedUser.getPhone());
		user.setLessonDuration(updatedUser.getLessonDuration());
		user.setLessonDay(updatedUser.getLessonDay());
		user.setPrice(updatedUser.getPrice());
		user.setSlot(updatedUser.getSlot());
        userRepo.save(user);
    }
    
	public void deleteRepById(Long id) {
		Rep rep = repRepo.findById(id).get();
        repRepo.delete(rep);
	}
	
    public void modifyRep(Long id, Rep updatedRep) {
    	System.out.println("modify");
		Rep rep = repRepo.findById(id).get();
        rep.setBeginDate(updatedRep.getBeginDate());
        rep.setCompositor(updatedRep.getCompositor());
        rep.setTitle(updatedRep.getTitle());
        repRepo.save(rep);
    }
    
	public Iterable<Lesson> findAllLessons() {
		return lessonRepo.findAll();
	}

	public void addLessonToUser(Long id, Lesson lesson) {
    	User user = userRepo.findById(id).get();
    	lesson.setUser(user);
    	user.getLessons().add(lesson);
        userRepo.save(user);
	}

	public void setLessonPayment(Long id, Lesson updatedLesson) {
    	User user = userRepo.findById(id).get();
        // Find the lesson to update by lesson ID
        Lesson lessonToUpdate = user.getLessons()
            .stream()
            .filter(lesson -> lesson.getId().equals(updatedLesson.getId()))
            .findFirst()
            .orElseThrow(() -> new EntityNotFoundException("Lesson not found with ID: " + updatedLesson.getId()));

        // Update the lesson properties
        lessonToUpdate.setIsPayed(updatedLesson.getIsPayed());

        userRepo.save(user);
	}
	
	public void setPaymentStatus(Long id, Number debt) {
    	User user = userRepo.findById(id).get();
    	user.setPaymentStatus(debt);
    	userRepo.save(user);
	}

	public void setGivenLessons(Long id, Number givenLessons) {
    	User user = userRepo.findById(id).get();
    	user.setGivenLessons(givenLessons);
    	userRepo.save(user);
	}
	
	
	
	public Set<Rep> findRepOfUser(Long id) {
    	User user = userRepo.findById(id).get();
    	Set<Rep> rep = user.getReps();
    	return rep;
	}
	
	public Rep findSingleRepOfUser(Long repId) {
		Rep rep = repRepo.findById(repId).get();
    	return rep;
	}

	
	public void addRepToUser(Long id, Rep rep) {
    	User user = userRepo.findById(id).get();
    	rep.setUser(user);
    	user.getReps().add(rep);
        userRepo.save(user);		
	}
	

	
}
