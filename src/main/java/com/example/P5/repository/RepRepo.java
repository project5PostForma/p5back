package com.example.P5.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.P5.entities.Lesson;
import com.example.P5.entities.Rep;

@Repository
public interface RepRepo extends JpaRepository<Rep, Long> {
	
	
}
