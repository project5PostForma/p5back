package com.example.P5.controllers;


import com.example.P5.entities.Lesson;
import com.example.P5.entities.User;
import com.example.P5.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4300", "http://localhost:8090", "http://piano-bucket1.s3-website.eu-west-3.amazonaws.com", "https://p5-back-service.onrender.com"} , maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api")

public class UserController {
	
	
	@Autowired
	private UserService userService;

	@GetMapping("/all")
	public Iterable<User> getAllUsers(){
		return userService.findAllUsers();
	}
	
	
	@GetMapping("/getUser/{id}")
	public User getUser(@PathVariable Long id){
		return userService.findUser(id);
	}

    @PostMapping("/addUser")
    public void addUser(@RequestBody User newUser) {
    	userService.addUser(newUser);
    }	
    
    @PutMapping("/modifyUser/{id}")
    public void modifyUser(@PathVariable Long id, @RequestBody User user) {
    	userService.modifyUser(id, user); 
    }
    
    @DeleteMapping("/deleteUserById/{id}")
    public void deleteUserById(@PathVariable Long id) {
    	userService.deleteUserById(id); 
    }
    
    @PutMapping("/setPaymentStatus/{id}")
    public void setPaymentStatus(@PathVariable Long id, @RequestBody Number debt) {
    	userService.setPaymentStatus(id, debt); 
    }
    
    @PutMapping("/setGivenLessons/{id}")
    public void setGivenLessons(@PathVariable Long id, @RequestBody Number givenLessons) {
    	userService.setGivenLessons(id, givenLessons); 
    }
}
