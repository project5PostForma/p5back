package com.example.P5.controllers;


import com.example.P5.entities.Lesson;
import com.example.P5.entities.Rep;
import com.example.P5.entities.User;
import com.example.P5.services.UserService;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4300", "http://localhost:8090", "http://piano-bucket1.s3-website.eu-west-3.amazonaws.com", "https://p5-back-service.onrender.com"} , maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api")

public class RepController {
	
	
	@Autowired
	private UserService userService;


	
	
	@GetMapping("/getUserRep/{id}")
	public Set<Rep> getUserRep(@PathVariable Long id){
		return userService.findRepOfUser(id);
	}
	
	@GetMapping("/getUserSingleRep/{repId}")
	public Rep getUserSingleRep(@PathVariable Long repId){
		return userService.findSingleRepOfUser(repId);
	}
	
    @PostMapping("/addRepToUser/{id}")
    public void addLessonToUser(@PathVariable Long id, @RequestBody Rep rep) {
    	System.out.println("######################################################");
    	System.out.println("######################################################");

    	System.out.println(id);
    	userService.addRepToUser(id, rep);
    }
    
    @PutMapping("/modifyRep/{id}")
    public void modifyRep(@PathVariable Long id, @RequestBody Rep rep) {
    	userService.modifyRep(id, rep); 
    }
    
    @DeleteMapping("/deleteRepById/{id}")
    public void deleteRepById(@PathVariable Long id) {
    	userService.deleteRepById(id); 
    }

}
