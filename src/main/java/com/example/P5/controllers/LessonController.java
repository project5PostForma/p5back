package com.example.P5.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.P5.services.UserService;
import com.example.P5.entities.Lesson;
import com.example.P5.entities.User;


@CrossOrigin(origins = {"http://localhost:4300", "http://localhost:8090", "http://piano-bucket1.s3-website.eu-west-3.amazonaws.com", "https://p5-back-service.onrender.com"} , maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api")

public class LessonController {
	
	
	@Autowired
	private UserService userService;

	@GetMapping("/allLessons")
	public Iterable<Lesson> getAllLessons(){
		return userService.findAllLessons();
	}
	

    @PostMapping("/addLessonToUser/{id}")
    public void addLessonToUser(@PathVariable Long id, @RequestBody Lesson lesson) {
    	userService.addLessonToUser(id, lesson);
    }	
    
    @PutMapping("/setLessonPayment/{id}")
    public void setLessonPayment(@PathVariable Long id, @RequestBody Lesson updatedLesson) {
    	userService.setLessonPayment(id, updatedLesson); 
    }
    

	

    
}
