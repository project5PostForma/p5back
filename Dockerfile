FROM openjdk:11.0-jre-slim

EXPOSE 8090

COPY target/*.jar /app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]




